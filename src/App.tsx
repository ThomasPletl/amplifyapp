import React from 'react';
import BookTable from './BookTable';
import bestBooks from './best-books';

function App() {
  return (
    <>
      <header>
        <h1>Bookworm</h1>
      </header>
      <main>
          <h2>The best books in the world</h2>
          <BookTable books={bestBooks}/>
      </main>
    </>
  );
}

export default App;
