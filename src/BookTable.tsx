import React from 'react';
import { Book } from './book';
import styles from './BookTable.module.css';

interface Props {
    books: Book[];
}

interface State {
    sortDesc: boolean;
    sortColumn: string;
    page: number;
    countShow: number;
    searchTerm: string;
    selLanguage: string;
}

export default class BookTable extends React.Component<Props, State> {
    constructor(props: Readonly<Props>) {
        super(props);
        this.state = {
            sortDesc: false,
            sortColumn: 'year',
            page: 1,
            countShow: 30,
            searchTerm: '',
            selLanguage: '',
        };
    }

    renderAuthorName(authorName: string) {
        return (authorName === 'Unknown' ? <td className={styles.unknown}>???</td> : <td>{authorName}</td>)
    }

    showInfo(link: any) {
        window.open(link)
    }

    renderTitle(book: Book) {
        if (book.link) {
            return <a href={book.link} target="_blank" rel="noopener noreferrer">{book.title}</a>
        }
        return book.title
    }

    setSort(clickedColumn: string) {

        this.setState({
            sortDesc: !this.state.sortDesc,
            sortColumn: clickedColumn
        })
    }

    usePaginator(direction: string) {
        let newPage = (direction === '+' ? this.state.page + 1 : this.state.page - 1)
        this.setState({
            page: newPage
        })
    }

    setSelection(searchTerm: string) {
        this.setState({
            searchTerm: searchTerm,
            page: 1
        })
    }

    handleChange(selectedValue: string) {
        this.setState({
            selLanguage: selectedValue === 'Sprache auswählen' ? '' : selectedValue,
        })
    }

    clearLangSel() {
        this.setState({
            selLanguage: '',
        })
    }


    render() {

        let books = this.props.books.map((book, index) => {
            return {
                ...book,
                key: index,
            }

        });

        if (this.state.sortDesc === false) {
            if (this.state.sortColumn === 'author') {
                books.sort((book1, book2) => (book1.author).localeCompare(book2.author))
            } else if (this.state.sortColumn === 'year') {
                books.sort((book1, book2) => book1.year - book2.year)
            } else if (this.state.sortColumn === 'title') {
                books.sort((book1, book2) => (book1.title).localeCompare(book2.title))
            } else if (this.state.sortColumn === 'pages') {
                books.sort((book1, book2) => book1.pages - book2.pages)
            } else if (this.state.sortColumn === 'language') {
                books.sort((book1, book2) => (book1.language).localeCompare(book2.language))
            }
        } else {
            if (this.state.sortColumn === 'author') {
                books.sort((book1, book2) => (book2.author).localeCompare(book1.author))
            } else if (this.state.sortColumn === 'year') {
                books.sort((book1, book2) => book2.year - book1.year)
            } else if (this.state.sortColumn === 'title') {
                books.sort((book1, book2) => (book2.title).localeCompare(book1.title))
            } else if (this.state.sortColumn === 'pages') {
                books.sort((book1, book2) => book2.pages - book1.pages)
            } else if (this.state.sortColumn === 'language') {
                books.sort((book1, book2) => (book2.language).localeCompare(book1.language))
            }
        }

        if (this.state.searchTerm !== '') {
            books = books.filter(b => b.title.toLowerCase().includes(this.state.searchTerm));
        }

        if (this.state.selLanguage !== '') {
            books = books.filter(b => b.language === this.state.selLanguage);
        }

        let languages = books.map(book => book.language)
            .filter((item, i, ar) => ar.indexOf(item) === i)
            .sort((lang1, lang2) => (lang1).localeCompare(lang2))
            .map(data => <option> {data} </option>);

        let maxPage = Math.floor(books.length / this.state.countShow) + (books.length % this.state.countShow > 0 ? 1 : 0);
        books = books.slice((this.state.page - 1) * this.state.countShow, (this.state.page) * this.state.countShow)

        return <div className="container">

            <div>Suche:&nbsp;<input type="text" onChange={(e) => this.setSelection(e.target.value)} /></div>

            <select onChange={(e) => this.handleChange(e.target.value)}>
                <option>Sprache ausw&auml;hlen</option>
                {languages}
            </select>
            <button onClick={() => this.clearLangSel()}>L&ouml;schen</button>

            <table className={styles.myTable}>
                <thead>
                    <tr>
                        <th onClick={() => this.setSort('author')}>Author</th>
                        <th onClick={() => this.setSort('year')}>Year</th>
                        <th onClick={() => this.setSort('title')}>Title</th>
                        <th onClick={() => this.setSort('pages')}>Pages</th>
                        <th onClick={() => this.setSort('language')}>Language</th>
                    </tr>
                </thead>
                <tbody>
                    {books.map(
                        book => <tr key={book.key}>
                            {this.renderAuthorName(book.author)}
                            <td>{book.year}</td>
                            <td>{this.renderTitle(book)}</td>
                            <td>{book.pages}</td>
                            <td>{book.language}</td>
                        </tr>
                    )}
                </tbody>
            </table>
            <div>{`Aktuelle Seite: ` + this.state.page + ` von ` + maxPage}</div>
            <div>
                {this.state.page > 1 ? <button onClick={() => this.usePaginator('-')}>Zur&uuml;ck</button> : null}
            &nbsp;
            {this.state.page < maxPage ? <button onClick={() => this.usePaginator('+')}>Vor</button> : null}
            </div>
        </div >
    }
}